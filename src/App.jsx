import { useEffect, useState } from 'react'
import './App.css'
import DemoComponent from './Demo_01_Component/containers/DemoComponent'
import DemoCSS from './Demo_02_CSS/DemoCSS'
import Condition from './Demo_03_Condition/Condition'
import DemoCollection from './Demo_04_Collections/DemoCollection'
import Evenement from './Demo_05_Evenements/Evenement'
import State from './Demo_06_State/State'
import Formulaire from './Demo_07_Formulaires/Formulaire'
import Parent from './Demo_08_DialogueComponents/Parent'
import Demo9 from './Demo_09_UseEffect/Demo9'
import Demo10 from './Demo_10_Ajax/Demo10'

const App = () => {
  const [ display, setDisplay ] = useState(false);

  useEffect(() => {
      console.log("Effet déclenché dans App !");
  }, [display] )
  // [] => Le UseEffect ne se déclenche qu'à l'apparition du composant mais si on modifie display, la fonction n'est pas exécutée
  // [display] => le UseEffect se déclenche à l'apparition ET si display change (mais s'il y avait une autre variable qui changeait, ça ne déclencherait pas le useEffect)

  const changeDisplay = () => {
    setDisplay(d => !d)
  }

  return (
    <>
      <h1 className="green">Bienvenue dans notre démo React</h1>
     
      {/* ------------ Demo 1 - Component ------------ */}
      <DemoComponent />

      {/* ------------ Demo 2 - CSS ------------ */}
      <DemoCSS />

      {/* ------------ Demo 3 - Rendu conditionnel ------------ */}
      <h2>Demo 3 - Rendu conditionnel</h2>
      {/* La personne n'est pas disponible */}
      <Condition available={false} vacation={false} name="Aurélien" />

      {/* La personne est disponible mais en vacances */}
      <Condition available={true} vacation={true} name="Sam" />

      {/* La personne est dispo, pas en vacances et a un nom */}
      <Condition available={true} vacation={false} name="Aude" />

      {/* La personne est dispo, pas en vacances mais n'a pas un nom */}
      <Condition available={true} vacation={false} />

      {/* ------------ Demo 4 - Rendu de collections ------------ */}
      <DemoCollection />

      {/* ------------ Demo 5 - Evènements ------------ */}
      <h2>Demo 5 - Evènements</h2>
      <Evenement />

      {/* ------------ Demo 6 - UseState ------------ */}
      <h2>Demo 6 - Hook d'état : UseState </h2>
      <State />

      {/* ------------ Demo 7 - Formulaires ------------ */}
      <h2>Demo 7 - Gestion formulaires </h2>
      <Formulaire />

      {/* ------------ Demo 8 - Dialogue Parent/Enfant ------------ */}
      <h2>Demo 8 - Dialogue Parent/Enfant </h2>
      <Parent />

      {/* ------------ Demo 9 - Cycle de vie - UseEffect() ------------ */}
      <h2>Demo 9 - Cycle de vie - UseEffect() </h2>
      <button onClick={changeDisplay}>Afficher Demo 9</button>
      { display && <Demo9 /> }

      {/* ------------ Demo 10 - Requêtes Ajax ------------ */}
      <h2>Demo 10 - Requêtes Ajax </h2>
      <Demo10 />

      <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />


    </>
  )
}

export default App
