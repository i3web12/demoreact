
const Enfant = (props) => {
    const {pizza, onSelectPizza} = props
    {/* pizza : Pour envoyer des données de parent vers enfant, on utilise les props */}

    //onSelectPizza : Event que l'enfant va déclenché, pour que le parent puisse réagir et recevoir potentiellement une information (Par convention, on met souvent on et un Verb pour illustrer que c'est un évènement)

    return (
        <div className="enfant">
            <h3>{pizza.name}</h3>
            <p>{pizza.price} €</p>
            <button onClick={() => { onSelectPizza(pizza) }}>Ajouter au panier</button>
        </div>
    )
}

export default Enfant;