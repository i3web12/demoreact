import { useState } from "react";
import Enfant from "./Enfant";
import "./ParentEnfant.css"

const Parent = () => {
    const produits = [
        { id : 1, name : "Pizza 4 fromages", price : 8.50 },
        { id : 2, name : "Pizza Margherita", price : 8 },
        { id : 3, name : "Pizza Parmigiana", price : 12.50 },        
    ]

    const [panier, setPanier] = useState([])

    const addToCart = (pizza /* envoyé via l'enfant au moment de l'appel de l'event onSelectPizza(pizza) */) => {
        console.log(pizza);
        //Vérifier si la pizza est dans le panier (vérifier si dans le panier, il y a un élément dont l'id = celui de la pizza cliquée)
        let prod = panier.find(p => p.id == pizza.id)
        //prod contient une pizza si trouvée, sinon undefined
        if(prod) {
            setPanier([...panier.map(p => {
                // si l'id de l'élément qu'on parcourt = id de la pizza cliquée
                if(p.id == pizza.id){
                    //On ajoute +1 à la qtt de l'élement parcouru
                    p.qtt++;
                    //On renvoie l'élément modifié
                    return p;
                }
                else {
                    //Sinon, on renvoie l'élément non modifié
                    return p;
                }
            })])
        }
        else {
            setPanier([...panier, {...pizza, qtt : 1}])

        }

    }

    return ( 
        <div className="parent">
            <h3>Composant parent</h3>
            <div>
                <h4>La carte :</h4>
                <div className="carte">
                    {/* Pour envoyer des données de parent vers enfant, on utilise les props */}
                    { produits.map(p => (<Enfant key={p.id} pizza={p} onSelectPizza={addToCart} />)) }
                </div>
            </div>

            <div>
                <h4>Votre panier :</h4>
                <table>
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Quantité</th>
                            <th>Prix</th>
                        </tr>
                    </thead>
                    <tbody>
                        { panier.map(elem => (
                            <tr key={elem.id}>
                                <td>{elem.name}</td>
                                <td>{elem.qtt}</td>
                                <td>{elem.price} €</td>
                            </tr>
                         )) }
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colSpan="2">Total : </td>
                            <td> { panier.reduce((total, prod) => total + (prod.qtt * prod.price), 0) } €</td>
                            {/* tab.reduce((accumulator, currentValue) => accumulator + currentValue, initialValue) */}
                            {/* accumulator -> la variable qui va recevoir le résultat du calcul tous les tours, currentValue -> la variable qu'on est en train de parcourir, initialValue -> La valeur de départ de l'accumulator*/}
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    )
}

export default Parent;