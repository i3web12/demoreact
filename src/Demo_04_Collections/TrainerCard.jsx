

// const TrainerCard = (props) => {
//     const { trainerToShow } = props;
//     // const trainerToShow = props.trainerToShow

//     return (
//         <div className="trainer-card"> 
//             <h4>{trainerToShow.firstname} {trainerToShow.lastname}</h4>
//             <img src={trainerToShow.avatar} />
//         </div>
//     )
// }

// Ou
// Façon 3 : Bonus 1

const TrainerCard = ( { trainerToShow } ) => {

    return (
        <div className="trainer-card"> 
            <h4>{trainerToShow.firstname} {trainerToShow.lastname}</h4>
            <img src={trainerToShow.avatar} />
        </div>
    )
}

// Ou
// Façon 3 : Bonus 2
// const TrainerCard = ( { firstname, lastname, avatar } ) => {
//     // Au lieu de recevoir les props et les destructurer à la main en dessous
//     // const { firstname, lastname, avatar } = props
//     // on destructure directement les props dans les paramètres du composant
//         return (
//             <div className="trainer-card"> 
//                 <h4>{firstname} {lastname}</h4>
//                 <img src={avatar} />
//             </div>
//         )
//     }
    

export default TrainerCard;