import CollectionAvancee from "./CollectionAvancee";
import CollectionSimple from "./CollectionSimple";

const DemoCollection = () => {
    return (
        <>
        <h2>Demo 4 - Rendu des collections</h2>
        <CollectionSimple />
        <CollectionAvancee />
        </>
    )
}

export default DemoCollection;