import TrainerCard from "./TrainerCard";

const CollectionAvancee = (props) => {
    
    const trainers = [
        { id : 1, lastname : "Beurive", firstname : "Aude", avatar : "src/assets/images/aude.png"},
        { id : 2, lastname : "Ly", firstname : "Khun", avatar : "src/assets/images/khun.png"},
        { id : 3, lastname : "Strimelle", firstname : "Aurélien", avatar : "src/assets/images/aurelien.png"}
    ]

    // Façon 2 :
    const trainersRender = trainers.map(trainer => ( 
        <div key={trainer.id}> 
            <h4>{trainer.firstname} {trainer.lastname}</h4>
            <img src={trainer.avatar} />
        </div>));

    return (
        <>
            <h3>Exemple avec une collection plus avancée</h3>
            <div className="trainers-container">

            
           
            {/* Façon 1 : Mais pas propre parce qu'on aura souvent beaucoup d'html dans le map et ce sera vite illisible*/}
            {/* { trainers.map(trainer => ( 
                <div key={trainer.id}> 
                    <h4>{trainer.firstname} {trainer.lastname}</h4>
                    <img src={trainer.avatar} />
                </div>))
            } */}

            {/* Façon 2 : Utiliser la constante vue pour collection simple */}
            {/* { trainersRender } */}

            {/* Façon 3 + Façon 3 Bonus 1*/}
            { trainers.map(trainer => <TrainerCard key={trainer.id} trainerToShow={trainer} />)}

            {/* Façon 3 : Bonus 2 */}
            {/* { trainers.map(trainer => <TrainerCard key={trainer.id} lastname={trainer.lastname} firstname={trainer.firstname} avatar={trainer.avatar} />)} */}
            </div>

        </>
    )

}

export default CollectionAvancee;