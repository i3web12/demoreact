


const CollectionSimple = () => {
    const days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
    console.log(days);

    // Deux façons de faire le rendu :
    // 1ère : Utiliser une constante dans laquelle on fait le map et c'est cette constante, qu'on met dans le rendu jsx
    // map => pour chaque day dans la liste days, je le transforme en une balise li avec la valeur du jour à l'intérieur
    // day : Vous pouvez mettre le nom que vous voulez, c'est le nom de la variable dans laquelle il y aura chacune des valeurs du tableau (le map parcours chacun des éléments comme le forEach)
    const daysRender = days.map((day, i) => ( <li key={i}>{day}</li> ) )
    
    console.log(daysRender);

    return (
        <>
            <h3>Exemple avec une collection simple</h3>
            <ul>
                {/* Façon 1 : */}
                {/* {daysRender} */}

                {/* Façon 2 : Faire le map directement dans la page et se passer de la constante */}
                { days.map((day, index) => ( <li key={index} >{day}</li> ) ) }
            </ul>
        </>
    )
}

export default CollectionSimple;