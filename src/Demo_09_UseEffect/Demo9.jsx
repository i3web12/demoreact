import { useEffect, useState } from "react";

const Demo9 = () => {
    const [count, setCount] = useState(0)

    useEffect(() => {
        //useEffect se déclenche à l'apparition du composant et chaque fois que count va changer
        console.log("Effet déclenché dans Demo9 !");
        document.title = "Count : " + count;

        // Nettoyage
        // Fonction appelée quand le composant disparait du Dom
        return () => {
            console.log("Nettoyage de Demo9 !");
        }
    }, [count])

    const add = () => {
        console.log(count);
        setCount(c => c + 1 )
    }

    return (
        <>
            <p>{count}</p>
            <button onClick={ add }> +1 </button>
        
        </>
    )
}

export default Demo9;