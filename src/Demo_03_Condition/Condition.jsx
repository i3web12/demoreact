
// available -> pour condition avec structure conditionnelle : Utilisé quand le rendu est drastiquement différent en fonction de la condition (if, switch)

// vacation -> pour condition avec opérateur ternaire : Utilisé directement dans le jsx (dans le rendu), pour des petits rendus conditionels

// name -> pour condition avec opérateur logique, pour des petits rendus conditionnels où on n'a pas besoin de faire une ternaire

const Condition = (props) => {
    const { available, vacation, name } = props

    // Si le formateur n'est pas dispo
    if(!available) {
        return (
            <>
                <p>Le formateur demandé n'est pas disponible 😢</p>
            </>
        )
    }

    // Si le formateur est dispo
    return (
        <>
            <p>Le formateur demandé est disponible 🥳</p>
            {/* { (condition) ? (renduvrai) : (rendufaux) } */}
            { 
                (vacation) ? 
                (<p>Il est malheureusement en vacances 🌞</p>) 
                : 
                (
                    // L'opérateur || affichera le contenu de la variable name si elle existe (si elle n'est pas undefined, si elle n'est pas null, si elle n'est pas false), sinon, le contenu à droite des ||
                    // <p>Il est à vous et son nom est { name || "inconnu" }</p>

                    // L'opérateur ?? affichera le contenu de la variable name si elle existe (si elle n'est pas undefined, si elle n'est pas null), sinon, le contenu à droite des ??
                    // <p>Il est à vous et son nom est { name ?? "inconnu" }</p>

                    // L'opérateur && affiche ce qu'il y a à droite des && si nom n'est pas undefined, null ou false
                    <p>Il est à vous et son nom est { name && "connu mais on vous dira pas cékoi" }</p>                    
                ) 
            }

        </>
    )
}

export default Condition;

