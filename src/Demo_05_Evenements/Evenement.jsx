
const Evenement = () => {

    let message = "en attente d'un bonjour...";

    const hello = () => {
        console.log("hello");

        // ! la variable message ne se mettra pas à jour dans le rendu, c'est normal, nous aurons des hooks, notamment celle pour gérer l'état du composant
        message = "Bonjour";
        console.log(message);
    }

    // Toute fonction reliée à un évènement DOM (onClick, onChange, etc etc etc) pourra recevoir dans les paramètres l'event déclenché
    const showInputValue = (event) => {
        console.log(event.target.value);
    }

    return (
        <>
        {/* On relie un évènement DOM au nom de la fonction qu'on a déclaré dans le composant */}
           <button onClick={hello} >Dire Bonjour</button>
           <div>{message}</div>

           <input type="text" placeholder="Tapez ce que vous voulez" onChange={showInputValue} />
        </>
    )

}

export default Evenement;