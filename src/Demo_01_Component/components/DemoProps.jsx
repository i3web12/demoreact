import PropTypes from 'prop-types'

const DemoProps = (props) => {
    //! Destructuring 
    // Pour pouvoir récupérer chacune des propriétés de l'objet props dans une variable, plutôt qu'écrire à chaque fois props.firstname, props.lastname etc...
    const { firstname, lastname, age } = props;

    return (
        <div>
            <p>Bonjour, je m'appelle {firstname} {lastname} et j'ai {age} ans.</p>
            {/* <p>Un p en commentaire</p> */}
        </div>
    )
}

//Définition des valeurs par défaut
//nomComposant.defaultProps = {}
DemoProps.defaultProps = {
    //nomProps : valeurProps
    firstname : 'Robert',
    lastname : 'Smith',
    age : 45
}

//Définition des types pour les props :
//? C'est un outil de développement, ça ne vous empêchera pas de mettre n'importe quoi dans les props mais ça vous mettra un warning dans la console

//! Attention la propriété de notre composant s'appelle propTypes sans majuscule à prop
//! Par contre, lorsqu'on définit le type, on va chercher la valeur dans l'objet PropTypes qu'on a extrait de la librairie (ligne 1) donc majuscule à P
DemoProps.propTypes = {
    //nomProp : typeAttendu
    firstname : PropTypes.string,
    lastname : PropTypes.string,
    age : PropTypes.number
}

export default DemoProps;