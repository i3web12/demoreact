//rafce pour le créer quand on a l'extension (React Arrow Fonction Component Export)

import DemoProps from "../components/DemoProps";


const DemoComponent = () => {
    
    return (
        <>
            <h2>Demo 1 - Les composants</h2>
            {/* <DemoProps></DemoProps> fonctionne aussi avec balises non orpheline */}

            <DemoProps firstname='Aude' lastname='Beurive' age={34} />

            <DemoProps firstname='Khun' lastname='Ly' age={41} />

            {/* Un component sans props, pour voir les props par défaut */}
            <DemoProps />

            {/* Rajouter les propsTypes mettra un message d'erreur dans la console pour nous prévenir qu'on n'a pas mis les bonnes valeurs MAIS le composant fonctionne quand même, ça n'empêche pas d'écrire n'importe quoi comme valeur */}
            <DemoProps firstname={12} lastname='Ly' age={true} />

        
        </>
    )

}

export default DemoComponent;

