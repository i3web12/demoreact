import { useState } from "react";
// useState est la Hook qui me permet de gérer l'état de mon composant, elle est déjà de base dans react

const State = () => {

    //Sans le state -> Ca marche pas 🤷‍♀️
    //let day = "[Veuillez selectionner un jour dans la liste]";

    const [day, setDay] = useState("[Veuillez selectionner un jour dans la liste]")
    //useState() renvoie un tableau avec 
        // dans la case 1 : La variable qu'on va utiliser dans notre composant
        // dans la case 2 : La fonction qui va nous permettre de modifier cette variable
    //Si un paramètre est fourni à useState, il s'agit de la valeur initiale de la variable

    const changeDay = (event) => {
        //day = event.target.value;

        //Modifie la valeur de la variable day
        //Si on a juste une valeur à réassigner, on peut la mettre directement dans les paramètres
        setDay(event.target.value); 

        // Si par contre, j'ai besoin de l'état précédent de la variable, je dois utiliser une fonction, qui aura comme paramètre, la valeur précédente
        //setDay(previousValue => previousValue + " " + event.target.value)
        
        console.log(day);
    }

    return (
        <>
            <p>Aujourd'hui nous sommes {day}</p>
            {/* <select onChange={changeDay}> */}
            <select onChange={(e) => { setDay(e.target.value) }}>
                <option value="lundi">Lundi</option>
                <option value="mardi">Mardi</option>
                <option value="mercredi">Mercredi</option>
                <option value="jeudi">Jeudi</option>
                <option value="vendredi">Vendredi</option>
                <option value="samedi">Samedi</option>
                <option value="dimanche">Dimanche</option>
            </select>
        </>
    )

}

export default State;