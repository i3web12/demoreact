
// Etape 1 : Installer Axios
// npm i axios

import axios from "axios"
import { useEffect, useState } from "react"

const Demo10 = () => {
    const [message, setMessage] = useState(null)
    const [error, setError] = useState(null)
    const [isLoading, setIsLoading] = useState(false)

    const [prenom, setPrenom] = useState("Aude")

    useEffect(() => {
        //Utilisé pour chargé un composant avec des valeurs quand il apparait 
        // Exemple : Votre page profil, une page qui affiche une liste de produit...
        searchAgeAsync();
    }, [])

    const searchAge = () => {
        setIsLoading(true);
        setError(null);
        setMessage(null);

        const url = `https://api.agify.io/?name=${prenom}&country_id=BE`;

        //axios.verb
        // get : récupérer des informations d'une API
        // post : envoyer des informations à notre API
        // put : modifier les informations d'un élément (remplace l'entièrement de l'élément)
        // patch : modifier les informations d'un élément (modifier une petite partie de l'élément)
        // delete : supprimer un élément
        setTimeout(() => {
            //setTimeout -> Juste pour simuler une requête un peu lente et voir notre loading
            axios.get(url)
                .then((response) => {
                    //Appelé quand la requête s'est envoyée
                    console.log("Requête envoyée");
                    console.log(response.data);
                    setMessage(`Votre âge probable est ${response.data.age} ans`)
                })
                .catch((error) => {
                    //Appelé quand la requête a échoué
                    console.log("Error requête");
                    console.log(error.response);
                    setError("Une erreur est survenue !")
                })
                .finally(() => {
                    //Appelé que la requête se soit envoyée ou ai échoué
                    console.log("Fin de la requête");
                    setIsLoading(false);
                })
        }, 1000)

    }

    const searchAgeAsync = async () => {
        setIsLoading(true);
        setError(null);
        setMessage(null);

        const url = `https://api.agify.io/?name=${prenom}&country_id=BE`;

        try {
            const response = await axios.get(url); //équivalent then
            setMessage(`Votre âge probable est ${response.data.age} ans`)
        }
        catch (error) {
            //équivalent catch
            if(error.response.status == 429) {
                setError("Trop de requêtes !")
            }
            else {
                setError("Une erreur est survenue !")
            }
        }
        //équivalent finally
        setIsLoading(false);

    }

    return (
        <div>
            <input type="text" placeholder="Votre nom" value={prenom} onChange={(e) => { setPrenom(e.target.value) }} />

            {/* C'est un évènement qui va déclencher l'appel API, on le gère alors directement une la fonction */}
            <button onClick={searchAgeAsync}>Rechercher</button>
            {isLoading ?
                <div> Chargement... </div>
                :
                error ?
                    <div> {error} </div>
                    :
                    message ?
                        <div> {message} </div>
                        :
                        <div> Entrez un prénom pour avoir une estimation de l'âge </div>}
        </div>
    )
}

export default Demo10