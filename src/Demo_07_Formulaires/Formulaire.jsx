import { useState } from "react";

const Formulaire = () => {
    // const [ obj, setObj ] = useState('');
    // const [ msg, setMsg ] = useState('');
    // const [ dest, setDest ] = useState('Khun');
    // const [ recept, setRecept ] = useState(false);

    // OU
    const [ inputs, setInputs ] = useState({ obj : '', msg : '', dest : 'Khun', recept : false, sign : 'Caroline' });

    const sendForm = (e) => {
        e.preventDefault(); //Annule l'évènement par défaut -> l'envoie sur l'action
        // console.log(obj, msg, dest, recept);
        console.log(inputs.obj, inputs.msg, inputs.dest, inputs.recept);
    }

    const changeValue = (e) => {
        const { checked, value, type, name } = e.target 
        // e.target -> élément html qui a déclenché l'event
        console.log(name, type, value, checked);
        // name contient la valeur de l'attribut name de l'input/textarea/select qui vient de déclencher l'event
        // type : le type de l'élément (text, number, select, textarea, checkbox etc)
        // value : La valeur encodée (si pas checkbox)
        // checked : La valeur booléènne (si checkbox)

        setInputs({ 
            ...inputs, //On récupère toutes les propriétés présentes dans l'objet inputs
            [name] : (type === 'checkbox') ? checked : value
            //Pour la propriété dont le nom est égal à celui de l'élément html récupéré, on vérifie si c'est un input type checkbox, si oui, on prend l'attribut checked, sinon value
         })
    }

    return (
        <form onSubmit={sendForm} >
            <div>
                <label htmlFor="obj">Objet du message :</label>
                {/* <input id="obj" type="text" value={obj} onChange={ (e) => { setObj(e.target.value)} } /> */}
                <input name="obj" id="obj" type="text" value={inputs.obj} onChange={ changeValue } />
            </div>
            <div>
                <label htmlFor="msg">Message :</label>
                {/* <textarea  id="msg" value={msg} onChange={ (e) => { setMsg(e.target.value)}} /> */}
                <textarea name="msg" id="msg" value={inputs.msg} onChange={ changeValue } />
            </div>
            <div>
                <label htmlFor="dest">Selectionner un destinataire :</label>
                {/* <select id="dest" value={dest} onChange={(e) => { setDest(e.target.value) }}> */}
                <select name="dest" id="dest" value={inputs.dest} onChange={changeValue}>
                    <option value="Aude">Aude</option>
                    <option value="Khun">Khun</option>
                    <option value="Quentin">Quentin</option>
                </select>
            </div>
            <div>
                {/* <input type="checkbox" id="recept" value={recept} onChange={ (e) => {setRecept(e.target.checked )}} /> */}
                <input name="recept" type="checkbox" id="recept" value={inputs.recept} onChange={changeValue} />
                <label htmlFor="recept">Souhaitez-vous recevoir un accusé de réception ?</label>
            </div>
            <div>
                <label htmlFor="sign">Signé : </label>
                <input name="sign" id="sign" type="text" value={inputs.sign} readOnly />
            </div>
            <div>
                <input type="submit" value="Envoyer" />
            </div>

        </form>

    )


}

export default Formulaire;