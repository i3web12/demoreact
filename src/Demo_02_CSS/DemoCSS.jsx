/* Méthode 1 : Via import d'un fichier CSS */
/* Ce css est global à toute l'application, attention si vous écrasez du style déjà existant, bah pas de bol */
import "./DemoCSS.css"

/* Méthode 2 : Via import d'un module CSS */
/* Attention ! On ne peut y mettre que des classes */
/* Créer la classe avec un hash (vite se charge de le faire pour nous), donc unique -> Aucun risque d'écraser une autre classe du même nom (ex : _yellow_aaq3l pour un et _yellow_ght84j pour l'autre) */
import style from './DemoCSS.module.css'
//style est un objet contenant toutes les classes présentes dans le module css
//On peut l'appeler comme on veut, par convention, c'est souvent style

import style2 from './DemoCSS2.module.css'

const DemoCSS = () => {

    return (
        <>
            <h2>Demo 2 - CSS</h2>
            <p className="green">Ce paragraphe est vert</p>
            <p className={style.yellow}>Ce paragraphe est jaune</p>
            <p className={style2.yellow}>Ce paragraphe est aussi jaune</p>
        </>

    ) 
}

export default DemoCSS;