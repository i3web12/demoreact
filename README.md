# Extensions pratiques :
* ES7+ React/Redux/React-Native snippets par dsznajder
* Material Icon Theme par Philipp Kief


# Pour créer un projet : 
```
    npm create vite@latest
    > Project Name -> Mettre le nom du projet
    (> si Package Name proposé -> Appuyez sur Enter)
    > Type projet -> React
    > Langage -> Javascript
```
Ensuite, ouvrez le projet dans VSCode ou faire cd nomProjet
```
    npm i
    ou
    npm install
    (Pour installer toutes les dépendances (librairies) dont le projet à besoin pour fonctionner)
```
Pour lancer le serveur :
```
    npm run dev
```

## Les symboles à côté des versions dans le package.json
Les deux les plus rencontrés : 
### tilde

The ~ means “approximate version.” This allows for more recent patches, but does not accept any packages with a different minor version. For example, ~2.3.3 will allow values between 2.3.3 and 2.4, not including 2.4. Note that this will allow differences in the minor version is none is specified. ~2 will accept any version that starts with 2.
### carat

The ^ means “compatible with version,” and is more broad than the tilde. It only refuses changes to the major version. For example, ^3.4.1 will allow any version between that value and 4.0.0, not including version 4. Note that this behaves very differently for version numbers that start with zero, so be careful.

### La liste complète : https://elliott-king.github.io/2020/08/package_json_symbols/

